#ifndef IOFILE_H
#define IOFILE_H
#include<iostream>
#include<string>
#include<stdlib.h>
#include<stdio.h>
#include<sstream>
#include<conio.h>
#include<fstream>
namespace FileWork{
    class File{
    public:
        static int WriteLine(const std::string Address,std::string _Value,bool AppendMode=false){
            std::ofstream W_L;
            if(AppendMode==true)
                W_L.open(Address.c_str(),std::ios_base::app);
            else
                W_L.open(Address.c_str());
            if(W_L.is_open()==true){
                W_L<<_Value;
                W_L.close();
                return 0;
            }
            else
                return 1;

        }
        static int RemoveFile(const std::string Address){
            int _R_=remove(Address.c_str());
            if(_R_!=0){
                perror("<ERROR>");
                return 1;
            }
            else{
                puts("File Successfully Deleted");
                return 0;
            }
        }
        static int RenameFile(const std::string Address,std::string NewName){
            int _R_=rename(Address.c_str(),NewName.c_str());
            if(_R_==0){
                puts("File Successfully Renamed");
                return 0;
            }
            else{
                perror("<ERROR>");
                return 1;
            }
        }
        static std::string ReadFile(const std::string Address,bool NoSpace=false){
            std::ifstream R_F(Address.c_str());
            std::string Result;
            std::string Returned;
            if(R_F.is_open()==true){
            	if(NoSpace==false)
                        std::getline(R_F,Result);
                else{
	                while(!R_F.eof()){
	                    R_F>>Result;
	                    Returned+=Result;
	                }
	            }
            }
            if(NoSpace==false)
                return Result;
            else
                return Returned;

        }

    };
}
#endif
